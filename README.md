Forces biblio to use literal names for authors after new biblio entries are added.

# Pre-requisites

* [biblio][https://www.drupal.org/project/bilbio]
* [hook_post_action][https://www.drupal.org/project/hook_post_action]

# Installation

* Download pre-requisite modules
* Download into Drupal modules path (eg. sites/all/modules or sites/example.com/modules)
* Enable with drush or through the Drupal interface

## Updating previous biblio entries

If you'd like to do a bulk-update of existing entries, it can be done directly in sql:

`
update biblio_contributor_data set literal=1 where literal=0;
`

# Copyright & License

Copyright 2018 Kienan Stewart <kienan@koumbit.org>
Copyright 2017 Mike Dean <mike@koumbit.org>

Licensed under GNU General Public License v2 or later (GPLv2+). See LICENSE.txt for
the full license text.
